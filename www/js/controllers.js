angular.module('starter.controllers', [])
    .controller('AppCtrl', function($scope, $ionicModal) {
    $scope.loginstatus = window.localStorage.getItem("isUserLoggedIn");
    $scope.displayname = window.localStorage.getItem("displayname");
    // Form data for the login modal
    $scope.loginData = {};


    var username = '';
  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope,
    animation: 'slide-in-up',
    focusFirstInput: true
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  $scope.logout = function() {
      window.localStorage.clear();
      window.location.reload(true);
  };

})
    .controller('PlaylistsCtrl', function($scope) {
      $scope.playlists = [
        { title: 'Reggae', id: 1 },
        { title: 'Chill', id: 2 },
        { title: 'Dubstep', id: 3 },
        { title: 'Indie', id: 4 },
        { title: 'Rap', id: 5 },
        { title: 'Cowbell', id: 6 }
      ];
    })



    .controller('PostsCtrl', function($scope, $http) {
        // You can change this url to experiment with other endpoints
        // var postsApi = 'http://wordpress.local/wp-json/posts?_jsonp=JSON_CALLBACK';
        var postsApi = 'http://vaydraalexander.com/api/get_posts';
        $http.post(postsApi).
        success(function(data, status, headers, config) {
            $scope.posts = data.posts;

        }).
        error(function(data, status, headers, config) {
            console.log( 'Post load error.' );
        });

    })

    .controller('PostCtrl', function($scope, $stateParams, $sce, $http ) {

        // we get the postID from $stateParams.postId, the query the api for that post
        var singlePostApi = 'http://vaydraalexander.com/?json=get_post&post_id='+$stateParams.postId;
        $scope.post = {};
        $http.post( singlePostApi ).
        success(function(data, status, headers, config) {
            $scope.post =   data.post;
            $scope.comment =   data.post.comments;
            $scope.reapter  = data.post.comment_count;
            $scope.post_id  = $stateParams.postId;
        }).
        error(function(data, status, headers, config) {
            console.log( 'Single post load error.' );
        });
    })

    .controller('PostCommentCtrl', function($scope, $http) {
        $scope.submit = function(){

            var comment = $scope.Comment.text;
            var userdata =  window.localStorage.getItem("authorizationToken");
            var name = JSON.parse(userdata).user.displayname;
            var email = JSON.parse(userdata).user.email;
            var postid = $scope.post_id;
            var publishcomment = 'http://vaydraalexander.com/?json=respond.submit_comment&post_id='+postid+'&content='+comment+'&name='+name+'&email='+email;

                    $http.post(publishcomment).then(function (publishcomment){
                            if(publishcomment.data.status == 'error'){
                                alert(publishcomment.data.error);
                                console.log(publishcomment.data.error);
                            }
                            else
                            {
                               alert("aaaaa");
                            }

                        });




        }

    })


    .controller('LoginCtrl', function($scope, $http) {
            $scope.data = {};
            $scope.submit = function(){
                //Generate User nonce
                    var link = 'http://vaydraalexander.com/api/get_nonce/?controller=auth&method=generate_auth_cookie';
                    $http.post(link).then(function (res){
                    $scope.response = res.data;
                        if($scope.response.nonce != ''){
                            console.log("Nonce created successfully.");
                            var username = $scope.loginData.username;
                            var nonce = $scope.response.nonce;
                            var password = $scope.loginData.password;
                            var login_link = 'http://vaydraalexander.com/api/auth/generate_auth_cookie/?nonce='+nonce+'&username='+username+'&password='+password+'&insecure=cool';
                            $http.post(login_link).then(function (reslogin){
                            if(reslogin.data.status == 'error'){
                                  alert(reslogin.data.error);
                                  console.log(reslogin.data.error);
                            }
                            else
                            {
                                window.localStorage.setItem("displayname", reslogin.data.user.username);
                                window.localStorage.setItem("authorizationToken", JSON.stringify(reslogin.data));
                                $scope.profile = JSON.parse(window.localStorage.getItem("authorizationToken"));
                                window.localStorage.setItem("isUserLoggedIn",true);
                                window.location.reload(true);
                            }

                            });
                        }
                        else{
                            console.log("Please try again later");
                            window.localStorage.setItem("isUserLoggedIn",false);
                            }
                });
            }
        })
    .controller('PlaylistCtrl', function($scope, $stateParams) {

    })


    .filter('htmlToPlaintext', function() {
          return function(text) {
          return String(text).replace(/<[^>]+>/gm, '');
          }
    });


















